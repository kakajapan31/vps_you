from selenium import webdriver


def get_chrome_driver(data: dict):
    chrome_options = webdriver.ChromeOptions()
    user_agent = data.get('user_agent')
    if user_agent:
        chrome_options.add_argument('--user-agent=%s' % user_agent)
    return webdriver.Chrome(data['driver_path'], chrome_options=chrome_options)


def get_firefox_driver(data: dict):
    if data.get('user_agent'):
        profile = webdriver.FirefoxProfile()
        profile.set_preference("general.useragent.override", data['user_agent'])
        firefox = webdriver.Firefox(executable_path=data['driver_path'], firefox_profile=profile)
        firefox.execute_script('return navigator.userAgent')
        return firefox
    else:
        return webdriver.Firefox(executable_path=data['driver_path'])


def get_driver(data: dict, broser_name: str):
    if broser_name == 'chrome':
        return get_chrome_driver(data)
    elif broser_name == 'firefox':
        return get_firefox_driver(data)
    else:
        return get_chrome_driver(data)
