import pandas
import json
import codecs


def to_json(file_name: str, browser_name: str):
    excel = pandas.read_excel(file_name)
    json_str = excel.to_json(orient='records')
    list_account = json.loads(json_str)
    for i in range(len(list_account)):
        account = list_account[i]
        file = codecs.open(f'account_profile/{browser_name}{i}.json', 'w', 'utf-8')
        if 'live_stream' in account:
            account['live_stream'] = (int(account['live_stream'] or 0) == 1)
        if 'time_to_see_all' in account:
            account['time_to_see_all'] = int(account['time_to_see_all'] or 7200)
        if 'cookies' in account:
            cookies = account['cookies'] or '[]'
            account['cookies'] = json.loads(cookies)

        if browser_name == 'chrome':
            account['driver_path'] = '../chromedriver'
        elif browser_name == 'firefox':
            account['driver_path'] = '../geckodriver'
        else:
            account['driver_path'] = '../chromedriver'

        file.write(json.dumps(account, ensure_ascii=False))


if __name__ == '__main__':
    to_json('account_chrome.xlsx', 'chrome')
    to_json('account_firefox.xlsx', 'firefox')
