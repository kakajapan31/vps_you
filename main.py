import json
import os
import random
import time
from threading import Thread

from driver import get_driver
from helpers import get_chrome_url
from settings import CHANNEL_LIST
from youtube import get_time_view_video, like_video, subscribe_channel, get_button_next_video, next_channel, \
    go_to_first_video, comment_video


def run_driver(data: dict, browser_name: str):
    while True:
        driver = get_driver(data, browser_name)
        data['channel_url'], data['channel_name'] = next_channel(None, CHANNEL_LIST)
        try:
            while True:
                print(f'{browser_name} is working at {data["channel_name"]}')
                has_login = get_chrome_url(driver, data)
                go_to_first_video(driver, data)
                counter = 0
                counter_max = random.randint(3660 * 2, 3600 * 3)
                while True:
                    print(f'{browser_name} is working at {driver.current_url}')
                    time_view = get_time_view_video(driver, data)
                    print(f'{browser_name} will be view in {time_view} seconds')

                    if has_login:
                        first = random.randint(time_view // 4, time_view // 4 * 2)
                        print(f'{browser_name} view video {driver.current_url} in {first} seconds before like')
                        time.sleep(first)
                        like_video(driver, data)

                        second = random.randint(time_view // 4 * 2, time_view // 4 * 3)
                        print(f'{browser_name} view video {driver.current_url} in {second - first} seconds subscribe')
                        time.sleep(second - first)
                        subscribe_channel(driver, data)

                        third = random.randint(time_view // 4 * 3, time_view)
                        print(f'{browser_name} view video {driver.current_url} in {third - second} seconds comment')
                        time.sleep(third - second)
                        # comment_video(chrome, 'haha', data)
                        time.sleep(time_view - third)
                    else:
                        time.sleep(time_view)

                    button_next = get_button_next_video(driver, data)
                    counter += time_view
                    if button_next and counter < counter_max:
                        button_next.click()
                    else:
                        data['channel_url'], data['channel_name'] = next_channel(data['channel_url'], CHANNEL_LIST)
                        break
                    print('\n' * 5)
        except Exception as e:
            print('chrome run fail so quit ', e)
        finally:
            driver.quit()


def run():
    threads = []

    for file in os.listdir('profile'):
        if file.endswith('.json'):
            file_dir = os.path.join('profile', file)
            browser_data = json.load(open(file_dir))
            if file.startswith('chrome'):
                chrome = Thread(target=run_driver, args=(browser_data, 'chrome'))
                threads.append(chrome)
            elif file.startswith('firefox'):
                firefox = Thread(target=run_driver, args=(browser_data, 'firefox'))
                threads.append(firefox)

    for t in threads:
        t.start()
    for t in threads:
        t.join()


if __name__ == '__main__':
    run()
