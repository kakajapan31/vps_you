import random
import time

import youtube_dl


def get_length_video(url: str, default=7200):
    try:
        ydl_opts = {
            'format': 'bestaudio/best',
            'outtmpl': 'tmp/%(id)s.%(ext)s',
            'noplaylist': True,
            'quiet': True,
            'prefer_ffmpeg': True,
            'audioformat': 'wav',
            'forceduration': True
        }
        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            res = ydl.extract_info(url, download=False)
            return res['duration']
    except:
        return default


def get_list_button_next_video(driver, data: dict):
    if data.get('user_agent'):
        list_button = driver.find_elements_by_xpath(
            '//*[@id="app"]/div[2]/ytm-watch/ytm-single-column-watch-next-results-renderer/ytm-item-section-renderer[2]/lazy-list/ytm-compact-video-renderer/div/div/a/h4/span')
    else:
        list_button = driver.find_elements_by_xpath('//span[@id="video-title"]')
    if len(list_button) > 0:
        list_button.pop()
    return list_button


def get_chrome_url(chrome, data: dict):
    cookies = data.get('cookies')
    url = data['channel_url']
    if cookies:
        get_url_with_cookies(chrome, cookies, url)
        return True
    else:
        get_url_without_cookies(chrome, url)
        return False


def get_url_with_cookies(driver, cookies, url):
    driver.delete_all_cookies()
    driver.get('https://www.youtube.com/')
    for cookie in cookies:
        try:
            driver.add_cookie(cookie)
        except:
            pass
    driver.get(url)
    time.sleep(1)
    driver.maximize_window()


def get_url_without_cookies(driver, url):
    driver.get(url)
    time.sleep(1)
    driver.maximize_window()


def play_video(driver):
    try:
        for i in range(10):
            driver.execute_script('document.getElementsByTagName("video")[0].play()')
            time.sleep(0.1)
    except:
        pass


def get_time_view(driver, data: dict):
    time_to_see_all = data.get('time_to_see_all', 7200)
    time_view_video = 8000
    if data.get('live_stream') is not True:
        time_video = get_length_video(driver.current_url)
        time_view_video = max(time_video - 150, 0)
    if time_view_video > time_to_see_all:
        time_view_of_user = time_to_see_all
    else:
        time_view_of_user = random.randint(data['min_time_view'], data['max_time_view'])

    return min(time_view_of_user, time_view_video)


def get_button_like_video(driver, data: dict):
    if data.get('user_agent'):
        button = driver.find_element_by_xpath(
            '//*[@id="app"]/div[2]/ytm-watch/ytm-single-column-watch-next-results-renderer/ytm-item-section-renderer[1]/lazy-list/ytm-slim-video-metadata-renderer/div[2]/c3-material-button[1]/button')
    else:
        button = driver.find_element_by_xpath(
            "//*[@id='top-level-buttons']/ytd-toggle-button-renderer[1]/a/yt-icon-button/button")
    liked = button.get_attribute('aria-pressed')

    return button, liked == 'false'


def get_button_subscribe_channel(driver, data: dict):
    need_click = False
    if data.get('user_agent'):
        button = driver.find_elements_by_xpath(
            '//*[@id="app"]/div[2]/ytm-watch/ytm-single-column-watch-next-results-renderer/ytm-item-section-renderer[1]/lazy-list/ytm-slim-video-metadata-renderer/ytm-slim-owner-renderer/div/ytm-subscribe-button-renderer/div/c3-material-button/button'
        )[-1]
        subscribed = button.get_attribute('aria-pressed')
        if subscribed == 'false':
            need_click = True
    else:
        button = driver.find_elements_by_xpath(
            '//*[@id="subscribe-button"]/ytd-subscribe-button-renderer/paper-button')[-1]
        subscribed = button.get_attribute('subscribed')
        if subscribed is None:
            need_click = True

    return button, need_click


if __name__ == '__main__':
    print(get_length_video('https://m.youtube.com/watch?v=iRHnO2WhHiM'))
