def get_channel_list():
    with open('channel_list.txt') as f:
        content = f.readlines()
    contents = [x.strip() for x in content]

    result = []
    for content in contents:
        x, y = content.split(' ', 1)
        tup = (x, y)
        result.append(tup)
    return result


CHANNEL_LIST = get_channel_list()
