import random
import time

from helpers import get_list_button_next_video, play_video, get_time_view, get_button_like_video, \
    get_button_subscribe_channel


def go_to_first_video(driver, data: dict):
    driver.get(data['channel_url'])
    if data.get('user_agent'):
        buttons = driver.find_elements_by_xpath('//*[@class="compact-media-item-headline"]')
    else:
        buttons = driver.find_elements_by_xpath('//a[@id="video-title"]')
    if len(buttons):
        x_ran = random.randint(5, 10)
        index = random.randint(0, min(len(buttons) - 1, x_ran))
        button = buttons[index]
        button.click()


def get_button_next_video(driver, data: dict):
    channel_name = data['channel_name']
    list_button = get_list_button_next_video(driver, data)
    for button in list_button or []:
        button_name = button.get_attribute('aria-label')
        if button_name is None or not isinstance(button_name, str):
            continue
        if channel_name in button_name:
            return button
    return None


def get_time_view_video(driver, data: dict = None):
    play_video(driver)
    time_view = get_time_view(driver, data)
    return time_view


def like_video(driver, data: dict = None):
    if not random.randint(0, 9):  # 10% probably to like
        button, need_click = get_button_like_video(driver, data)
        if need_click:
            print(f'to like video {driver.current_url}')
            button.click()
            time.sleep(10)


def subscribe_channel(driver, data: dict = None):
    if not random.randint(0, 19):  # 5% probably to subscribe
        button, need_click = get_button_subscribe_channel(driver, data)
        if need_click:
            print('to subscribe channel')
            button.click()
            time.sleep(10)


def comment_video(driver, comment, data: dict = None):
    for i in range(100, 800, 100):
        driver.execute_script(f"window.scrollTo(0, {i})")
        time.sleep(0.1)
    time.sleep(3)
    print(data.get('proxy_url'), f'to comment {comment}')
    driver.find_element_by_id('simplebox-placeholder').click()
    driver.find_element_by_id('contenteditable-root').send_keys(comment)
    driver.find_element_by_xpath('//*[@id="submit-button"]').click()
    time.sleep(10)


def next_channel(channel, channels: list):
    if len(channels) <= 1:
        return channels[0]
    while True:
        result = channels[random.randint(0, len(channels) - 1)]
        if channel is None or channel[0] != result[0]:
            return result
